# Standup Notes

## Requirements
### Technical
- `pipenv` must be installed.

### Credentials
- An Atlassian account with access to a Site with Confluence Cloud.
- An API Token for your Atlassian account.
    - You should be able to [manage your API Tokens here.](https://id.atlassian.com/manage-profile/security/api-tokens).

### Structure of the Confluence Page
It is expected that the structure of the Confluence Page being checked follows _at least_ this structure:

- @ExamplePersonA
    - Today  # <-- could be any text
        - To-do item

A structure such as this is also supported...:

- @ExamplePersonA
    - Yesterday
        - Item1
        - Item2
        - Item3
    - Today
        - Item1
        - Item2
    - Blockers
        - BlockedItem1
        - BlockedItem2
- @ExamplePersonB
    - Yesterday
        - Item1
        - Item2
    - Today
    - Blocked
        - None
- @ExamplePersonC
    - Yesterday
        - Item1
    - Today
        - Item1
    - Blocked
        - None

...in which case @ExamplePersonB would get flagged as having incomplete notes.



## Getting Started
1. `pipenv install`
1. `pipenv run check-completion.py`

This will prompt you for the required Atlassian API connection credentials, as well as the title and Space of the Confluence Page that you would like to be checked.

Optionally, you can just `cp settings.example.yml settings.yml` and fill _that_ in if you would like to 'save' your information for repeated use versus being prompted each time.



## Notes
This script does not save any information, and should you decide to contribute you'll be happy to know that `settings.yml` is in `.gitignore`.

In short, your credentials are made no less safe by using this script.

If you feel that your host machine is already compromised, maybe skip `settings.yml` and stick to the prompts. Oh, and maybe get un-compromised. :P
