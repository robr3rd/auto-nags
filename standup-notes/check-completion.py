#! /usr/bin/env python
#
# Check for Standup Notes Completion (and offer to automatically "nag" until completed)
#
# Usage: pipenv run check-completion.py
# Arguments: None.  All user preferences are managed from settings.yml.


# Modules for Jira interactions
import requests      # for API requests
from requests.auth import HTTPBasicAuth  # for API requests
import json          # for API responses

# Modules for credentials/settings
import os.path       # check if settings file exists
from os import path  # (same thing)
import yaml          # settings file
import getpass       # interactive (sensitive) input (if no settings file)

# Modules for local script processing
from lxml import html      # for parsing response's HTML
from datetime import date  # for pre-filling today's date for the Page title

# Debugging tools
# from pprint import pprint
#print(html.tostring(<Element goes here>))  # <- show element's HTML



# Settings - handle settings/arguments/user input
SETTINGS = yaml.full_load(open('settings.example.yml'))  # load defaults first

if path.isfile('settings.yml'):  # if custom file exists
    SETTINGS.update(yaml.full_load(open('settings.yml')))  # override defaults


## Auth
site = SETTINGS['site'] or input('Subdomain of Confluence Cloud instance: ')
user = SETTINGS['user'] or input('Username for ' + site + '.atlassian.net: ')
token = SETTINGS['token'] or getpass.getpass('Personal API Token: ')
auth = HTTPBasicAuth(user, token)


## Query parameters
spaceKey = SETTINGS['spaceKey'] or input('Confluence Space Key: ')
title = SETTINGS['title'] or input('Title of Confluence Page (press ENTER for today): ') or date.today().strftime('%Y-%m-%d Standup')



## This script's settings
### People
people = SETTINGS['people'] or json.loads(input('Please enter the name and email of all people (ex: \'{"jsmith@exmaple.com":"John Smith","jdoe@example.com":"Jane Doe"}\'): '))

### Will draft email?
if SETTINGS['draft_email'] is None:  # unspecified
	will_draft_email = input("Would you like to notify them via email? (y/N) ")
	will_draft_email = True if will_draft_email in ['y', 'Y', 'yes', 'YES'] else False
elif type(SETTINGS['draft_email']) != bool:  # specified (but wrong)
	exit('ERROR: "draft_email" must be a boolean.  "' + SETTINGS['draft_email'] + '" given.')
else:  # specified (and valid)
	will_draft_email = SETTINGS['draft_email']

### Will send draft?
if will_draft_email:
	if SETTINGS['send_draft'] is None:  # unspecified
		will_send_draft = input("Would you like to send it now or review the draft first? (y=send; N=draft) ")
		will_send_draft = True if will_send_draft in ['y', 'Y', 'yes', 'YES'] else False
	elif type(SETTINGS['send_draft']) != bool:  # specified (but wrong)
		exit('ERROR: "send_draft" must be a boolean.  ' + type(SETTINGS['send_draft']) + '"' + SETTINGS['send_draft'] + '" given.')
	else:  # specified (and valid)
		will_send_draft = SETTINGS['send_draft']



# Functions

def request_page_content(auth, site, spaceKey, title):
	base_url = 'https://' + site + '.atlassian.net/wiki/rest/api/'

	response = requests.get(
		base_url + 'content',
		headers={'Accept': 'application/json'},
		auth=auth,
		params={
			'spaceKey': spaceKey,
			'title': title,
			'type': 'page',
			'expand': 'body.view'
		}
	)
	#print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
	return json.loads(response.text)['results'][0]['body']['view']['value']


def process_content(content):
	people_missing_notes = []

	root = html.fromstring(content)
	people = root.xpath('li')
	for person in people:
		person_name = person.xpath('p')[0].text_content()
		print('- ' + person_name)

		if 'PTO' in person_name:
			continue

		topics = person.xpath('ul/li')
		for topic in topics:
			topic_name = topic.xpath('p')[0].text_content()
			print("\t- " + topic_name)

			items = topic.xpath('ul/li')

			item_qty = len(items)
			if item_qty == 0:
				people_missing_notes.append(person_name)
				continue

			for item in items:
				item_text = item.xpath('p')[0].text_content()
				print("\t\t- " + item_text)

	return people_missing_notes


def draft_email(recipient_names, people, send_draft=False):
	command = "osascript draft-email.AppleScript '"  # this is the "base" command

	key_list = list(people.keys())
	val_list = list(people.values())
	for recipient_name in recipient_names:
		recipient_name = recipient_name.strip(' ')  # trim excess whitespace

		recipient_pos_in_people = val_list.index(recipient_name)
		recipient_email = key_list[recipient_pos_in_people]  # email pos == name pos

		command += recipient_email + ':' + recipient_name + ';'

	command = command.rstrip(';') + "' '" + str(send_draft).lower() + "'"

	print("\nRunning the following command: " + command + "\n")
	os.system(command)



# Execute
content = request_page_content(auth, site, spaceKey, title)
people_missing_notes = list(set(process_content(content)))

if people_missing_notes:
	print("\nThe following people are missing standup notes:")
	for person in people_missing_notes:  # list(set()) = unique list
		print('- ' + person)

	if will_draft_email:
		draft_email(people_missing_notes, people, will_send_draft)
