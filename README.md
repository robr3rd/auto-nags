# Auto-Nags
Things that need a follow-up that don't **really** need a **human** to perform it.

## License
See [LICENSE](LICENSE) ("BSD-2-Clause").
